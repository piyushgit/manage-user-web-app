import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const getUserFeed = (userAuthToken) => {
    return axios.get(`${baseUrl}/users/me/profile?token=${userAuthToken}`);
};