import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./Home";
import LoginPage from "./login/LoginPage";
import SignupPage from "./signup/SignupPage";
import { PrivateRoute } from './PrivateRoute';
import ForgotPasswordPage from "./forgotPassword/ForgotPasswordPage";
import UserOverviewPage from "./profile/overview/UserOverviewPage";
import ChangePasswordPage from "./profile/changePassword/ChangePasswordPage";
import EditProfilePage from "./profile/editProfile/EditProfilePage";
import SignupConfirmationPage from "./signupConfirmation/SignupConfirmationPage";
import SignupConfirmation from "./signupConfirmation/SignupConfirmation";

const Main = () => (
    <main className="p50">
        <Switch>
            <PrivateRoute exact path="/" component={Home} />
            <Route path="/login" component={LoginPage} />
            <Route path="/signup" component={SignupPage} />
            <Route path="/forgot-password" component={ForgotPasswordPage} />
            <Route path="/forgot-password:token" component={ForgotPasswordPage}/>
            <Route path="/register/resend-email:resendEmail" component={SignupConfirmationPage}/>
            <Route path="/register/verify:token" component={SignupConfirmationPage} />
            <PrivateRoute path="/change-password" component={ChangePasswordPage}/>
            <PrivateRoute path="/account-overview" component={UserOverviewPage} />
            <PrivateRoute path="/edit-profile" component={EditProfilePage} />
        </Switch>
    </main>
);

export default Main;
