import React from 'react';
import PropTypes from 'prop-types';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import { FieldFeedbacks, FormGroup, FormControlLabel, FormControlInput } from 'react-form-with-constraints/lib/Bootstrap4';
import Message from '../alert/Message';


class ForgotPasswordResetForm extends React.Component 
{
    constructor (props)
    {
        super(props);
        this.state = {
            password: '',
            passwordConfirm: '',
            message:'',
            error: false,
            checkPattern: '',
            isLoading: false
        };
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.formWithConstraints = this.formWithConstraints.bind(this);
        this.whenNumber = this.whenNumber.bind(this);
        this.whenSmallLetters = this.whenSmallLetters.bind(this);
        this.whenCapitalLetters = this.whenCapitalLetters.bind(this);
        this.whenSpecial = this.whenSpecial.bind(this);
        this.whenSamePassword = this.whenSamePassword.bind(this);
        this.innerRef = this.innerRef.bind(this);
    }

    onChange(e)
    {
        const target = e.currentTarget;

        this.form.validateFields(target);

        this.setState(
            {
            [target.name]: target.value,
            isLoading: !this.form.isValid()
            }
        );
    }

    onPasswordChange(e) 
    {
        this.form.validateFields('passwordConfirm');
        this.onChange(e);
    }

    onSubmit(e) 
    {
        e.preventDefault();

        this.form.validateFields();

        this.setState({ isLoading: !this.form.isValid() });

         if(this.form.isValid())
        {
            this.props.forgotPassword({"token":this.props.token, "passwordConfirm":this.state.passwordConfirm}).then(
                (response)=>{this.setState({message:response.data.message, error: false});},
                (error)=>(error.response.status==401)?this.setState({error: true, message:error.response.data.error}):this.setState({error: true, message:'Server Error'})
            );
        }
       
    }

    formWithConstraints(formWithConstraints){
        this.form = formWithConstraints;
    }

    innerRef(password){
        return this.password = password;
    }

    whenNumber(value){
        return !/\d/.test(value);
    }

    whenSmallLetters(value){
        return !/[a-z]/.test(value);
    }

    whenCapitalLetters(value){
        return !/[A-Z]/.test(value);
    }

    whenSpecial(value){
        return !/\W/.test(value);
    }
    
    whenSamePassword(value){
        return value !== this.password.value;
    }

    render()
    {
        return(
                
        <div className="col-md-6 col-lg-6 col-sm-12">
            {(this.state.message!='')?<Message message={this.state.message} error={this.state.error}/>:''}
            <div className="card py-4 px-4">
                <div className="card-block">
                    <h3 className="text-center">Reset Password</h3>
                    <FormWithConstraints ref={this.formWithConstraints}
                            onSubmit={this.onSubmit} noValidate>
                        <FormGroup for="password">
                        <FormControlLabel htmlFor="password" className="h5">New Password</FormControlLabel>
                        <FormControlInput type="password" id="password" name="password"
                            innerRef={this.innerRef}
                            value={this.state.password} onChange={this.onPasswordChange} placeholder="Enter password"
                            required pattern=".{5,}" />
                        <FieldFeedbacks for="password" show="all">
                            <FieldFeedback when="valueMissing" />
                            <FieldFeedback when="patternMismatch">Should be at least 5 characters long</FieldFeedback>
                            <FieldFeedback when={this.whenNumber} error>
                                Should contain numbers
                            </FieldFeedback>
                            <FieldFeedback when={this.whenSmallLetters} error>
                                Should contain small letters
                            </FieldFeedback>
                            <FieldFeedback when={this.whenCapitalLetters} error>
                                Should contain capital letters
                            </FieldFeedback>
                            <FieldFeedback when={this.whenSpecial} error>
                                Should contain special characters
                            </FieldFeedback>
                        </FieldFeedbacks>
                    </FormGroup>

                    <FormGroup for="passwordConfirm">
                        <FormControlLabel htmlFor="password-confirm" className="h5">Confirm Password</FormControlLabel>
                        <FormControlInput type="password" id="password-confirm" name="passwordConfirm"
                            value={this.state.passwordConfirm} placeholder="Confirm password" onChange={this.onChange} />
                        <FieldFeedbacks for="passwordConfirm">
                            <FieldFeedback when={this.whenSamePassword}>Not the same password</FieldFeedback>
                        </FieldFeedbacks>
                    </FormGroup>
                        <button disabled={this.state.isLoading}  className="btn btn-block btn-success">Submit</button>
                    </FormWithConstraints>
                </div>
            </div>
        </div>
        );
    }
}

ForgotPasswordResetForm.propTypes = {
    forgotPassword: PropTypes.func.isRequired,
    token:PropTypes.string.isRequired
};

export default ForgotPasswordResetForm;