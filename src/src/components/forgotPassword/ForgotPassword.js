import React from 'react';
import PropTypes from 'prop-types';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import {
  FieldFeedbacks,
  FormGroup,
  FormControlLabel,
  FormControlInput
} from 'react-form-with-constraints/lib/Bootstrap4';
import Message from '../alert/Message';

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      isLoading: false,
      message: '',
      error: false
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.formRef = this.formRef.bind(this);
  }

  onChange(e) {
    const target = e.currentTarget;

    this.form.validateFields(target);

    this.setState({
      [target.name]: target.value,
      isLoading: !this.form.isValid()
    });
  }

  onSubmit(e) {
    e.preventDefault();

    this.form.validateFields();

    this.setState({ isLoading: true });

    if (this.form.isValid()) {
      return this.props.forgotPasswordRequest({ email: this.state.email }).then(
        response => this.setState({ message: 'An email to confirm this password change request has been sent to your email. This might take a few minutes. Check your spam folder if you do not receive the email within few minutes.', error: false }),
        err => {
          err.response.status == 500
            ? this.setState({ message: 'Server error', error: true })
            : this.setState({ message: err.response.data, error: true });
        }
      );
    }
  }

  formRef(ref) {
    this.form = ref;
  }

  render() {
    return (
      <div className="container ">
        <div className="row justify-content-md-center my-4">
          <div className="col-md-6 col-lg-6 col-sm-12">
            {this.state.message != '' ? <Message message={this.state.message} error={this.state.error} /> : ''}
            <div className="card py-4 px-4">
              <div className="card-block">
                <h3 className="text-center">Forgot Password</h3>
                <div>
                  Enter your email address that you used to register. We'll send you an email with a link to reset your
                  password.
                </div>
                <br />
                <FormWithConstraints ref={this.formRef} onSubmit={this.onSubmit} noValidate>
                  <FormGroup for="email">
                    <div className="input-group">
                      <span className="input-group-addon">
                        <i className="fa fa-envelope-o" />
                      </span>
                      <FormControlInput
                        type="email"
                        id="email"
                        name="email"
                        placeholder="Enter email"
                        value={this.state.email}
                        onChange={this.onChange}
                        required
                        minLength={3}
                      />
                    </div>
                    <FieldFeedbacks for="email">
                      <FieldFeedback when="valueMissing">
                        Please enter your email address that you used to register.
                      </FieldFeedback>
                      <FieldFeedback when="tooShort">Too short</FieldFeedback>
                      <FieldFeedback when="*" />
                    </FieldFeedbacks>
                  </FormGroup>
                  <button disabled={this.state.isLoading} className="btn btn-block btn-success">
                    Send
                  </button>
                </FormWithConstraints>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  forgotPasswordRequest: PropTypes.func
};

export default ForgotPassword;
