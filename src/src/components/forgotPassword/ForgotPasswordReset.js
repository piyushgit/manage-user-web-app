import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import ForgotPasswordResetForm from './ForgotPasswordResetForm';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import { FieldFeedbacks, FormGroup, FormControlLabel, FormControlInput } from 'react-form-with-constraints/lib/Bootstrap4';
import Message from '../alert/Message';

import config from '../../data/env';
const baseUrl = config[0].userApiUrl;

class ForgotPasswordReset extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: '',
            error: true,
            token: this.props.token
        };
    }

    componentWillMount() {
        let token = this.state.token.replace(':', '');
        axios.get(`${baseUrl}/users/password/verifytoken/${token}`).then(
            (response) => this.setState({ error: false }),
            (error) => (error.response.status == 401) ? this.setState({ error: true, message: error.response.data.error }) : this.setState({ error: true, message: 'Server Error' })
        );
    }

    render() {
        return (
            <div className="container ">
                <div className="row justify-content-md-center my-4">
                    {(this.state.message != '') ? <Message message={this.state.message} error={this.state.error} /> : <ForgotPasswordResetForm token={this.props.token} forgotPassword={this.props.forgotPassword} />}
                </div>
            </div>
        );
    }
}

ForgotPasswordReset.propTypes = {
    token: PropTypes.string.isRequired,
    forgotPassword: PropTypes.func
};

export default ForgotPasswordReset;