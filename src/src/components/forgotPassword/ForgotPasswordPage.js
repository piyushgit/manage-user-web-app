import React from 'react';
import PropTypes from 'prop-types';
import ForgotPassword from './ForgotPassword';
import ForgotPasswordReset from './ForgotPasswordReset';
import {forgotPasswordRequest, forgotPassword} from '../../actions/forgotPasswordActions';
import {connect} from 'react-redux';
 
class ForgotPasswordPage extends React.Component 
{
    render() 
    {
        const {forgotPassword, forgotPasswordRequest, match} = this.props;
        let token = match.params.token;
        return (
            <div className="p100">
                {(token)?<ForgotPasswordReset token={token} forgotPassword={forgotPassword} />:<ForgotPassword forgotPasswordRequest={forgotPasswordRequest}/>}
            </div>
        );
    }
}

ForgotPasswordPage.propTypes = 
{
    forgotPassword:PropTypes.func.isRequired,
    forgotPasswordRequest:PropTypes.func.isRequired,
    match: PropTypes.object
};

export default connect(null, {forgotPasswordRequest, forgotPassword})(ForgotPasswordPage);