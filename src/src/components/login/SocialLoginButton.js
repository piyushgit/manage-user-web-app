import React from 'react';
import PropTypes from 'prop-types';

class SocialLoginButton extends React.Component {
  constructor() {
    super();
  }

  render() {
    const { loginUrl, socialLoginProvider, iconClas } = this.props;
    return (
      <div className="row py-2">
        <div className="col-md-12">
          <a href={loginUrl} className={`btn btn-block btn-social btn-lg btn-${iconClas}`}>
            <span className={`fa fa-${iconClas}`} />
            {socialLoginProvider} Login
          </a>
        </div>
      </div>
    );
  }
}

SocialLoginButton.propTypes = {
  loginUrl: PropTypes.string.isRequired,
  iconClas: PropTypes.string.isRequired,
  socialLoginProvider: PropTypes.string.isRequired
};

export default SocialLoginButton;
