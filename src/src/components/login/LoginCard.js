import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import Message from '../alert/Message';
import {
  FieldFeedbacks,
  FormGroup,
  FormControlLabel,
  FormControlInput
} from 'react-form-with-constraints/lib/Bootstrap4';
import { setAuthorizationToken } from '../../shared/setAuthorizationToken';
import { ClipLoader } from 'react-spinners';

class LoginCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoading: false,
      redirect: false,
      redirectTo: '',
      userLogged: '',
      message: '',
      loading: false,
      error: false
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.formWithConstraints = this.formWithConstraints.bind(this);
  }

  componentWillMount() {
    let userAuthToken = localStorage.getItem('userAuthToken');
    if (userAuthToken) {
      this.setState({ redirect: true });
    }
  }

  onChange(e) {
    const target = e.currentTarget;
    let check = target.value;
    if (check == '') {
      this.setState({ error: '' });
    }
    this.form.validateFields(target);

    this.setState({
      [target.name]: target.value,
      isLoading: !this.form.isValid()
    });
  }

  /* refForm(formWithConstraints)
     {
      return this.form =  formWithConstraints;   
     }*/

  onSubmit(e) {
    e.preventDefault();
    this.form.validateFields();
    this.setState({ isLoading: true });
    let userToken = '';
    if (this.form.isValid()) {
      this.setState({loading: true});
      userToken = this.props.userLoginRequest({ email: this.state.email, password: this.state.password }).then(
        res => {
          userToken = res.data.success.token;

          //setAuthorizationToken(userToken);

          if (userToken != '') {
             
            this.props
              .userLoggedDetail(userToken)
              .then(
              res => this.setState({ redirect: true, loading: false }),
              err => this.setState({ errors: err.response.data.errors, isLoading: false, loading: false })
              );
          }
        },
        err => {
          (err.response.status == 500 || err.response.status == 404)
            ? this.setState({ message: 'Server error', error: true, isLoading: false, loading:false }):(err.response.status == 422)?this.setState({redirectTo:`register/resend-email:${this.state.email}`, isLoading: false, loading:false})
            : this.setState({ message: 'User name or password is invalid.', isLoading: false, loading:false, error: true });
        }
      );
    }
  }
  
  formWithConstraints(formWithConstraints) {
    this.form = formWithConstraints;
  }

  render() {
    const { email, password, isLoading, redirect, redirectTo } = this.state;

    if (redirect) {
      return <Redirect to="/" />;
    }

    if (redirectTo != '') {
      return <Redirect to={`/${redirectTo}`}/>;
    }
    return (
      <div className="card-block">
        <FormWithConstraints
          ref={this.formWithConstraints}
          onSubmit={this.onSubmit}
          noValidate
        >
          <h3 className="text-center">Login</h3>
          {this.state.error != '' ? <Message message={this.state.message} error={this.state.error} /> : ''}
          <FormGroup for="email" className="pt-2">
            <div className="input-group">
              <span className="input-group-addon">
                <i className="fa fa-envelope-o fa-fw" />
              </span>
              <FormControlInput
                type="email"
                id="email"
                name="email"
                placeholder="Enter email"
                value={this.state.email}
                onChange={this.onChange}
                required
                minLength={3}
              />
            </div>
            <FieldFeedbacks for="email">
              <FieldFeedback when="tooShort">Too short</FieldFeedback>
              <FieldFeedback when="valueMissing">Please enter your email address.</FieldFeedback>
              <FieldFeedback when="*" />
            </FieldFeedbacks>
          </FormGroup>

          <FormGroup for="password">
           <div className="input-group">
            <span className="input-group-addon">
              <i className="fa fa-key fa-fw" />
            </span>
            <FormControlInput
              type="password"
              id="password"
              name="password"
              placeholder="Enter password"
              value={this.state.password}
              onChange={this.onChange}
              required
            />
            </div>
            <FieldFeedbacks for="password" show="all">
              <FieldFeedback when="valueMissing">Please enter your password.</FieldFeedback>
            </FieldFeedbacks>
          </FormGroup>
          <button disabled={this.state.isLoading} className="btn btn-block btn-success">Login
            <ClipLoader color={'#123abc'} loading={this.state.loading} />
          </button>
        </FormWithConstraints>

        <div className="row pt-3">
          <div className="col-md-12">
            <p className="text-center h6">
              <Link to="forgot-password" className="link-style-none">
                Forgot your password?
              </Link>
            </p>
          </div>
        </div>

        <div className="row pt-2 ">
          <div className="col-sm-2 mr_divider px-0">
            <hr className="hr_divider" />
          </div>
          <div className="col-sm-8 ml_divider px-0">
            <p className="text-center h6">Don't have an account?</p>
          </div>
          <div className="col-sm-2 ml_divider px-0">
            <hr className="hr_divider" />
          </div>
        </div>

        <div className="row pt-2">
          <div className="col-md-12">
            <Link to="signup" className="btn btn-primary btn-block">
              Sign Up
            </Link>
            <p className="text-center h4 pt-2">Or</p>
          </div>
        </div>
      </div>
    );
  }
}

LoginCard.propTypes = {
  userLoginRequest: PropTypes.func,
  userLoggedDetail: PropTypes.func,
  alertActions: PropTypes.func
};

export default LoginCard;
