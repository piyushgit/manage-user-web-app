import React from 'react';
import PropTypes from 'prop-types';
import LoginForm from './LoginForm.js';
import {connect} from 'react-redux';
import {userLoginRequest, userLoggedDetail} from '../../actions/loginActions.js';
 
class LoginPage extends React.Component 
{
    render() 
    {
        const {userLoginRequest, userLoggedDetail, alertConstants} = this.props;
        return (
            <div className="row py-4 justify-content-md-center">
                <div className="col-lg-3 col-md-4 col-sm-12">
                    <LoginForm userLoginRequest={userLoginRequest} userLoggedDetail={userLoggedDetail}/>
                </div>
            </div>    
        );
    }
}

LoginPage.propTypes = 
{
    userLoginRequest:PropTypes.func.isRequired,
    userLoggedDetail:PropTypes.func.isRequired,
    alertConstants:PropTypes.func
};

export default connect(null, {userLoginRequest, userLoggedDetail})(LoginPage);