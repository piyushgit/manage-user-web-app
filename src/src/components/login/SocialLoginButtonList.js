import React from 'react';
import PropTypes from 'prop-types';
import SocialLoginButton from './SocialLoginButton';

import config from '../../data/env';
const baseUrl = config[0].userApiUrl;

class SocialLoginButtonList extends React.Component {
  constructor() {
    super();
  }

  render() {
    let i = 1;
    let socicalLoginList = this.props.socialLoginData.map(data => <SocialLoginButton key={i++} {...data} />);

    return (
      <div>
        <div className="row px-4">
          <div className="col-md-12">
          </div>
        </div>
        {socicalLoginList}
      </div>
    );
  }
}

SocialLoginButtonList.defaultProps = {
  socialLoginData: [
    {
      loginUrl: `${baseUrl}/login/twitter`,
      iconClas: 'twitter',
      socialLoginProvider: 'Twitter'
    },
    {
      loginUrl: `${baseUrl}/login/linkedi`,
      iconClas: 'linkedin',
      socialLoginProvider: 'Linkedin'
    },
    {
      loginUrl: `${baseUrl}/login/faceboo`,
      iconClas: 'facebook',
      socialLoginProvider: 'Facebook'
    },
    {
      loginUrl: `${baseUrl}/login/google`,
      iconClas: 'google',
      socialLoginProvider: 'Google'
    }
  ]
};

SocialLoginButtonList.propTypes = {
  socialLoginData: PropTypes.array.isRequired
};

export default SocialLoginButtonList;
