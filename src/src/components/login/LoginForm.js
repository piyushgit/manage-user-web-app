import React from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router';
import LoginCard from './LoginCard';
import SocialLoginButtonList from './SocialLoginButtonList';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card py-2 px-4">
                <LoginCard userLoginRequest={this.props.userLoginRequest} userLoggedDetail={this.props.userLoggedDetail}/>
                <SocialLoginButtonList />
            </div>
        );
    }
}
LoginForm.propTypes = 
{
    userLoginRequest:PropTypes.func.isRequired,
    userLoggedDetail:PropTypes.func.isRequired
};
export default LoginForm;