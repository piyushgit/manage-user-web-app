import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import SignupConfirmation from './SignupConfirmation';
import {signupConfirmationVerify, resendSignupVefification} from '../../actions/signupActions';

class SignupConfirmationPage extends SignupConfirmation{
    render(){
        const { match: { params}, signupConfirmationVerify, resendSignupVefification } = this.props;
        const token = (params.token)?params.token.replace(':', ''):'';
        const resendEmail = (params.resendEmail)?params.resendEmail.replace(':', ''):'';
        return(
            <SignupConfirmation token={token} resendEmail={resendEmail} resendSignupVefification={resendSignupVefification} signupConfirmationVerify={signupConfirmationVerify} />
        );
    }

} 

export default connect(null, {signupConfirmationVerify, resendSignupVefification})(SignupConfirmationPage);