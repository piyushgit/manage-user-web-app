import React from 'react';
import PropTypes from 'prop-types';
import Message from '../alert/Message';

class SignupConfirmation extends React.Component{

    constructor(props){
        super(props);
        this.state={
            token: props.token,
            message: '',
            error: false,
            isLoading: false
        };
        this.onResendEmail = this.onResendEmail.bind(this);
    }

    componentWillMount(){
        const {token} = this.state;
        if(token){
            this.props.signupConfirmationVerify(this.state.token).then(
            (response)=>{this.setState({error: false, message: 'Email is successfully verified' });},
            (error)=>{(error.response.status == 500)?this.setState({error: true, message: 'Server error please try again'}):this.setState({error: true, message: error.response.data.error});}
        );
        }
        
    }

    onResendEmail() {
        
        const {resendEmail} = this.props;

        this.props.resendSignupVefification(resendEmail).then(
            response=>this.setState({message:`Email is resend to ${resendEmail}`, error: false}),
            error=>(error.response.status == 500 || error.response.status == 404)?this.setState({message:`Unable to send email to ${resendEmail} please try later.`, error:true}):this.setState({message:`Unable to send email to ${resendEmail}.`, error:true})
        );
        this.setState({isLoading: true});
    }

    render(){
        const {message, error, isLoading} = this.state;
        const {resendEmail} = this.props;

        const resendRequest = (
            <div className="card text-center px-4 py-4">
                <div className="card-block">
                    <h4 className="card-title">Verify your account email address</h4>
                    <p className="card-text">If you didn‘t receive your verification  please click the button to resend email verification to your account.</p>

                    <button className="btn btn-primary" disabled={isLoading?true:false} onClick={this.onResendEmail}>Resend email verification</button>
                </div>
            </div> 
        );
        
        return(
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6 pt-4 text-center">
                    {this.state.message?<Message message={message} error={error}/>:''}   
                    {(resendEmail)?resendRequest:''} 
                    </div>
                </div>
            </div>
        );
    }
}

SignupConfirmation.propTypes = {
    token: PropTypes.string,
    resendEmail: PropTypes.string,
    signupConfirmationVerify: PropTypes.func.isRequired,
    resendSignupVefification: PropTypes.func.isRequired
};

export default SignupConfirmation;