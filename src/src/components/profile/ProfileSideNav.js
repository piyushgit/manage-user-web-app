import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {NavLink} from 'react-router-dom';
import { logout } from '../../actions/loginActions';

class ProfileSideNav extends React.Component{

    constructor(props) {
    super(props);
        this.state={
            nav:''
        };
        this.logout = this.logout.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    closeMenu(){
      this.setState({nav: 'close'});
    }

    logout(e) {
    let userAuthToken = localStorage.getItem("userAuthToken");
    e.preventDefault();
    this.props.logout(userAuthToken);
    }

    render(){
        
        const {navStatus} = this.props;
        
        //console.log(this.props.navStatus);
        let user = JSON.parse(localStorage.getItem('loggedUser'));
        return (
        <nav className={`col-sm-12 col21 hidden-xs-down ubg-inverse sidebar ${(navStatus=='open')?'hiddenShow':''}`}>
            <div className="profile-userpic">
                <img src="#" className="img-responsive" alt=""/>
            </div>

            <div className="profile-usertitle">
                <div className="profile-usertitle-name">
                    {user.firstName+' '+user.lastName}
                </div>
                <div className="profile-usertitle-job"/>
            </div>

          <ul className="nav nav-pills flex-column">
          <li className="divider"></li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/account-overview" onClick={this.closeMenu}><i className="fa fa-home iSideNav" aria-hidden="true"></i> Account overview <span className="sr-only">(current)</span></NavLink>
            </li>
            <li className="divider"></li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/edit-profile" onClick={this.closeMenu}><i className="fa fa-pencil iSideNav" aria-hidden="true"></i> Edit profile</NavLink>
            </li>
            <li className="divider"></li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/change-password" onClick={this.closeMenu}><i className="fa fa-lock iSideNav" aria-hidden="true"></i> Change password</NavLink>
            </li>
            <li className="divider"></li>
               <li className="nav-item">
              <NavLink className="nav-link" to="#" onClick={this.logout}><i className="fa fa-sign-out iSideNav" aria-hidden="true"></i> logout</NavLink>
            </li>
            <li className="divider"></li>
          </ul>
        </nav>
        );
    }

}

ProfileSideNav.propTypes = {
    logout: PropTypes.func,
    navStatus: PropTypes.string
};

export default connect(null, {logout}) (ProfileSideNav);