import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import {
  FieldFeedbacks,
  FormGroup,
  FormControlLabel,
  FormControlInput
} from 'react-form-with-constraints/lib/Bootstrap4';
import Message from '../../alert/Message';
import { ClipLoader } from 'react-spinners';

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPassword: '',
      password: '',
      passwordConfirm: '',
      isLoading: false,
      error: false,
      message: '',
      loading: false
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.formWithConstraints = this.formWithConstraints.bind(this);
    this.whenNumber = this.whenNumber.bind(this);
    this.whenSmallLetters = this.whenSmallLetters.bind(this);
    this.whenCapitalLetters = this.whenCapitalLetters.bind(this);
    this.whenSpecial = this.whenSpecial.bind(this);
    this.whenSamePassword = this.whenSamePassword.bind(this);
    this.innerRef = this.innerRef.bind(this);
  }

  onChange(e) {
    const target = e.currentTarget;

    let check = target.value;
    if (check == '') {
      this.setState({ error: '', message: '' });
    }

    this.form.validateFields(target);

    this.setState({
      [target.name]: target.value,
      isLoading: !this.form.isValid()
    });
  }

  onPasswordChange(e) {
    this.form.validateFields('passwordConfirm');
    this.onChange(e);
  }

  onSubmit(e) {
    e.preventDefault();

    this.form.validateFields();

    this.setState({ isLoading: true});

    if (this.form.isValid()) {
       this.setState({ isLoading: true, loading:true });
      this.props
        .passwordChange({ oldPassword: this.state.currentPassword, passwordConfirm: this.state.passwordConfirm })
        .then(
          response => this.setState({ message: 'Password changed successfully.', error: false, isLoading: false, loading:false }),
          err => {
            err.response.data.status == 500
              ? this.setState({ message: 'Server error', error: true, isLoading: false, loading:false })
              : this.setState({ message: 'Current password is invalid', error: true, isLoading: false, loading:false });
          }
        );
    }
  }

  formWithConstraints(formWithConstraints)   
  {
    this.form = formWithConstraints;
  }

  innerRef(password){
    return this.password = password;
  }

  whenNumber(value){
    return !/\d/.test(value);
  }

  whenSmallLetters(value){
    return !/[a-z]/.test(value);
  }

  whenCapitalLetters(value){
    return !/[A-Z]/.test(value);
  }

  whenSpecial(value){
    return !/\W/.test(value);
  }
  
  whenSamePassword(value){
    return value !== this.password.value;
  }

  render() {
    return (
      <div className="row">
        <div className="col-sm-12 ml-sm-auto col-md-12 pt-3">
          <div className="page-wrapper">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-sm-12 ml-sm-auto col-md-12">
                    <h1 className="display-4">Change password</h1>
                    <hr className="one" />

                    <div className="row">
                      <div className="col-md-12 col-sm-12">
                        {this.state.message != '' ? <Message message={this.state.message} error={this.state.error} /> : ''}
                        <FormWithConstraints
                          ref={this.formWithConstraints}
                          onSubmit={this.onSubmit}
                          noValidate
                        >
                        <FormGroup for="currentPassword">
                          <div className="row">
                            <div className="col-sm-12 col-md-3 col-lg-3">
                              <FormControlLabel htmlFor="currentPassword" className="h5 text-muted">
                                Current Password
                              </FormControlLabel>
                            </div>
                              <div className="col-sm-12 col-md-9 col-lg-9">
                                <FormControlInput
                                  type="password"
                                  id="currentPassword"
                                  name="currentPassword"
                                  value={this.state.currentPassword}
                                  onChange={this.onChange}
                                  placeholder="Enter current password"
                                  required
                                  minLength={3}
                                />
                                <FieldFeedbacks for="currentPassword">
                                  <FieldFeedback when="tooShort">Too short</FieldFeedback>
                                  <FieldFeedback when="*" />
                                </FieldFeedbacks>
                              </div>
                              
                            </div>
                          </FormGroup>
                          <hr/>

                          <FormGroup for="password">
                            <div className="row">
                              <div className="col-sm-12 col-md-3 col-lg-3">
                                <FormControlLabel htmlFor="password" className="h5 text-muted">
                                  New password
                                </FormControlLabel>
                              </div>
                              <div className="col-sm-12 col-md-9 col-lg-9">
                                <FormControlInput
                                  type="password"
                                  id="password"
                                  name="password"
                                  innerRef={this.innerRef}
                                  value={this.state.password}
                                  onChange={this.onPasswordChange}
                                  placeholder="Enter new password"
                                  required
                                  pattern=".{5,}"
                                />
                                <FieldFeedbacks for="password" show="all">
                                  <FieldFeedback when="valueMissing" />
                                    <FieldFeedback when="patternMismatch">Should be at least 5 characters long</FieldFeedback>
                                    <FieldFeedback when={this.whenNumber} error>
                                      Should contain numbers
                                    </FieldFeedback>
                                    <FieldFeedback when={this.whenSmallLetters} error>
                                      Should contain small letters
                                    </FieldFeedback>
                                    <FieldFeedback when={this.whenCapitalLetters} error>
                                      Should contain capital letters
                                    </FieldFeedback>
                                    <FieldFeedback when={this.whenSpecial} error>
                                      Should contain special characters
                                  </FieldFeedback>
                                </FieldFeedbacks>
                              </div>
                            </div>
                          </FormGroup>
                          <hr/>

                          <FormGroup className="pb-4" for="passwordConfirm">
                            <div className="row">
                              <div className="col-sm-12 col-md-3 col-lg-3">
                                <FormControlLabel htmlFor="password-confirm" className="h5 text-muted">
                                  Repeat new password
                                </FormControlLabel>
                              </div>
                              <div className="col-sm-12 col-md-9 col-lg-9">
                                <FormControlInput
                                  type="password"
                                  id="password-confirm"
                                  name="passwordConfirm"
                                  value={this.state.passwordConfirm}
                                  placeholder="Repeat new password"
                                  onChange={this.onChange}
                                />
                              <FieldFeedbacks for="passwordConfirm">
                                <FieldFeedback when={this.whenSamePassword}>Not the same password</FieldFeedback>
                              </FieldFeedbacks>
                              </div>
                            </div>
                          </FormGroup>
                          <hr />
                          
                        <div className="row pt-2">
                          <div className="col-md-8">
                            <p className="h6 text-right lh1 text-uppercase">
                              {' '}
                              <NavLink to="/account-overview">Cancel</NavLink>
                            </p>
                          </div>
                          <div className="col-md-4">
                            <button
                              type="submit"
                              disabled={this.state.isLoading}
                              className="btn btn-success btn-block text-uppercase"
                            >
                            Update Password
                            <ClipLoader color={'#123abc'} loading={this.state.loading} />
                            </button>
                          </div>
                        </div>
                      </FormWithConstraints>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  passwordChange: PropTypes.func.isRequired
};

export default ChangePassword;
