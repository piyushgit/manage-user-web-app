import React from 'react';
import PropTypes from 'prop-types';

import ChangePassword from './ChangePassword';
import {connect} from 'react-redux';
import {passwordChange} from '../../../actions/profileActions';
import ProfileSideNav from '../ProfileSideNav';

class ChangePasswordPage extends React.Component {

    render()
    {
        const {passwordChange} = this.props;
        return ( 
            
            <div className="container">
            
                <div className="row">
                    <div className="col-md-3 col-sm-12 col-lg-3 hidden-xs">
                        <ProfileSideNav />
                    </div>
                    
                    <div className="col-md-9 col-sm-12 col-lg-9">
                        <ChangePassword passwordChange={this.props.passwordChange}/> 
                    </div>
                </div>
                
            </div>
          
            
        );
    }

}

ChangePasswordPage.propTypes = {
    passwordChange: PropTypes.func.isRequired
};

export default connect(null, {passwordChange})(ChangePasswordPage);