import React from 'react';

//import {bindActionCreators} from 'redux';
//import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import UserOverview from './UserOverview';
import ProfileSideNav from '../ProfileSideNav';

class UserOverviewPage extends React.Component
{
    render(){
       
        return (
            <div className="container container-content">
                <div className="row">
                    <div className="col-md-3 col-sm-12 col-lg-3 hidden-xs">
                        <ProfileSideNav />
                    </div>
                    <div className="col-md-9 col-sm-12 col-lg-9">
                        <UserOverview />
                    </div>
                </div>
            </div>
        );
    }
}

UserOverviewPage.propTypes = {
    users: PropTypes.object
};

export default UserOverviewPage;