import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

class UserOverview extends React.Component{

    render(){
        let user = JSON.parse(localStorage.getItem('loggedUser'));
        return(
            <div className="row">
                <div className="col-sm-12 ml-sm-auto col-md-12 pt-3">
                    <div className="page-wrapper">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-sm-12 col-md-12 ">
                                    <h1 className="display-4">Account Overview</h1>
                                    <hr className="one"/>
                                    <div className="row">
                                        <div className="col-12"><h5>Profile</h5></div>
                                        <div className="col-md-12">  
                                            <hr/>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-4 col-lg-4">
                                                    <p className="h5 text-muted">Email</p>
                                                </div>
                                                <div className="col-sm-12 col-md-8 col-lg-8">
                                                    <p className="h5">{user.email}</p>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-4 col-lg-4">
                                                    <p className="h5 text-muted">User name</p>
                                                </div>
                                                <div className="col-sm-12 col-md-8 col-lg-8">
                                                    <p className="h5">{user.firstName+' '+user.lastName}</p>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-4 col-lg-4">
                                                    <p className="h5 text-muted">Date of birth</p>
                                                </div>
                                                <div className="col-sm-12 col-md-8 col-lg-8">
                                                    <p className="h5">{`${user.dayBirth}/${user.monthBirth}/${user.yearBirth}`}</p>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row">
                                                <div className="col-sm-12 col-md-4 col-lg-4">
                                                    <p className="h5 text-muted">Gender</p>
                                                </div>
                                                <div className="col-sm-12 col-md-8 col-lg-8">
                                                    <p className="h5">{user.gender==2?`Male`:user.gender==3?`Female`:'Non-Binary'}</p>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div className="row pt-4 justify-content-md-center">
                                                <div className="col-md-12 text-center">
                                                    <NavLink to="/edit-profile"  className="btn btn-success text-uppercase button-area1">Edit profile</NavLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default UserOverview;
