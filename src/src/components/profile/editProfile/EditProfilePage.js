import React from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import EditProfile from './EditProfile';

import {editProfile} from '../../../actions/profileActions';
import  {setCurrentUser} from '../../../actions/loginActions';

class EditProfilePage extends React.Component{
    render()
    {
        const {user, setCurrentUser, editProfile} = this.props;
        return(
            <div className="container">
                <EditProfile user={this.props.user} setCurrentUser={setCurrentUser} editProfile={editProfile}/>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    let userDetail = JSON.parse(localStorage.getItem('loggedUser'));
    return({
        user: userDetail
    });
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({setCurrentUser, editProfile}, dispatch);
};

EditProfilePage.propTypes = {
   user: PropTypes.object.isRequired,
   editProfile: PropTypes.func.isRequired,
   setCurrentUser: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage);
