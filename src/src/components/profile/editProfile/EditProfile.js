import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import { FieldFeedbacks, FormGroup, FormControlLabel, FormControlInput } from 'react-form-with-constraints/lib/Bootstrap4';
import Message from '../../alert/Message';
import ProfileSideNav from '../ProfileSideNav';
import FormSelectField from '../../common/FormSelectField';
import FormRadioField from '../../common/FormRadioField';
import { ClipLoader } from 'react-spinners';

class EditProfile extends React.Component {

    constructor(props) 
    {
        super(props);

        const {user} = this.props;

        this.state =
            {
                dayBirth: user.dayBirth,
                monthBirth: user.monthBirth,
                yearBirth: user.yearBirth,
                gender: user.gender,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                termsConditions: false,
                valid: true,
                error: false,
                message: '',
                isLoading: false,
                loading: false
            };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.selectMonth = this.selectMonth.bind(this);
        this.selectYear = this.selectYear.bind(this);
        this.selectDay= this.selectDay.bind(this);
        this.selectGender = this.selectGender.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.formWithConstraints = this.formWithConstraints.bind(this);
    }

    onChange(e) 
    {
        const target = e.currentTarget;

        let check = target.value;
        if(check == ''){
            this.setState({error: '', message: ''});
        }

        this.form.validateFields(target);

        this.setState(
            {
                [target.name]: target.value,
                isLoading: !this.form.isValid()
            }
        );
    }

    onPasswordChange(e) 
    {
        this.form.validateFields('passwordConfirm');
        this.onChange(e);
    }

    selectMonth(selectMonth) 
    {
        this.setState({monthBirth: selectMonth, valid:true});
    }

    selectDay(selectDay)
    {
        this.setState({dayBirth: selectDay, valid:true});
    }

    selectYear(selectYear)
    {
        this.setState({yearBirth: selectYear, valid:true});
    }

    selectGender(selectGender) 
    {
        this.setState({gender: selectGender});
    }

    onSubmit(e) 
    {
        const {firstName, lastName, email, error, monthBirth, yearBirth, dayBirth, gender, message, isLoading} = this.state;

        (gender!='')?this.setState({gender:gender}):this.setState({gender:null});
        (monthBirth=='')?this.setState({valid: false}):this.setState({valid: true}) ;
        
        e.preventDefault();

        this.form.validateFields();
        this.setState({isLoading: true});

        let token = localStorage.getItem('userAuthToken');
        
        if (this.form.isValid() && gender != null && monthBirth != '' && yearBirth != '' && dayBirth != '') 
        {
            this.setState({isLoading: true, loading:true});
            const dob = `${this.state.yearBirth}-${this.state.monthBirth}-${this.state.dayBirth}`;

            this.props.editProfile({firstName, lastName, dob, gender}).then(
            (response)=>{localStorage.setItem('loggedUser', JSON.stringify({email, firstName, lastName, dayBirth, monthBirth, yearBirth, gender })); this.setState({firstName: firstName, lastName: lastName, message:'Successfully edited profile', error: false, isLoading: false, loading:false});},
            (error)=>{
                 error.response.status == 500
            ? this.setState({ message: 'Server error', error: true, loading: false, isLoading:false })
            : this.setState({ message: error.response.data.error, error: true, loading: false, isLoading:false });
            }
            ) ;
        }
    }

    formWithConstraints(formWithConstraints)   
    {
        this.form = formWithConstraints;
    }

    render(){
        const {firstName, lastName, email, error, message, monthBirth, dayBirth, yearBirth, isLoading} = this.state;

        return(
       
        <div className="row">
            <div className="col-md-3 col-sm-12 col-lg-3">
                <ProfileSideNav user={{email, firstName, lastName}}/>
            </div>
            <div className="col-md-9 col-sm-12 col-lg-9">
                <div className="row">
                    <div className="col-sm-12 ml-sm-auto col-md-12 pt-3">
                        <div className="page-wrapper">
                            <div className="container-fluid">  
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 ">
                                        <h1 className="display-4">Edit profile</h1>
                                        <hr className="one"/>
                                        <div className="row">
                                            <div className="col-md-12 col-sm-12"> 
                                                {(message!='')?<Message message={message} error={error}/>:''}
                                                <FormWithConstraints ref={this.formWithConstraints}
                                                    onSubmit={this.onSubmit} noValidate>
                                                    
                                                    <fieldset className="form-group">
                                                        <div className="row">
                                                            <div className="col-sm-12 col-md-3 col-lg-3">
                                                                <label className="h5 text-muted" htmlFor="CurrentPassword">Email</label>
                                                            </div>
                                                            <div className="col-sm-12 col-md-9 col-lg-9">
                                                                <input type="text" className="form-control" name="email" value={email} disabled/>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                    <hr/>
                                                    <FormGroup for="FirstName">
                                                        <div className="row">
                                                            <div className="col-sm-12 col-md-3 col-lg-3">
                                                                <FormControlLabel htmlFor="first-name" className="h5 text-muted">First Name</FormControlLabel>
                                                            </div>
                                                            <div className="col-sm-12 col-md-9 col-lg-9 text-left">
                                                                <FormControlInput type="text" id="firstName" name="firstName"
                                                                value={firstName} onChange={this.onChange} placeholder="Enter first name"
                                                                required minLength={3} />
                                                                <FieldFeedbacks for="firstName">
                                                                    <FieldFeedback when="tooShort">Too short</FieldFeedback>
                                                                    <FieldFeedback when="*" />
                                                                </FieldFeedbacks>
                                                            </div>
                                                        </div>
                                                    </FormGroup>
                                                    <hr/>
                                                    <FormGroup  for="lastName">
                                                        <div className="row">
                                                            <div className="col-sm-12 col-md-3 col-lg-3">
                                                                <FormControlLabel htmlFor="lastName" className="h5 text-muted">Last Name</FormControlLabel>
                                                            </div>
                                                            <div className="col-sm-12 col-md-9 col-lg-9">
                                                                <FormControlInput type="text" id="lastName" name="lastName"
                                                                value={lastName} onChange={this.onChange} placeholder="Enter last name"
                                                                required minLength={3} />
                                                                <FieldFeedbacks for="lastName">
                                                                    <FieldFeedback when="tooShort">Too short</FieldFeedback>
                                                                    <FieldFeedback when="*" />
                                                                </FieldFeedbacks>
                                                            </div>
                                                        </div>
                                                    </FormGroup>
                                                    <hr/>
                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-3 col-lg-3">
                                                            <label className="h5 text-muted" htmlFor="CurrentPassword">Date of birth</label>
                                                        </div>

                                                        <div className="col-sm-12 col-md-9 col-lg-9">
                                                            <div className="row">
                                                                <div className="col-sm-12 col-md-3 col-lg-3">
                                                                    <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectDay} selectedValue={dayBirth} operation="dayBirth"/>
                                                                </div>

                                                                <div className="col-sm-12 col-md-5 col-lg-5">
                                                                    <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectMonth} selectedValue={monthBirth} operation="monthBirth"/>
                                                                </div>  
                                                            
                                                                <div className="col-sm-12 col-md-4 col-lg-4">
                                                                    <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectYear} selectedValue={yearBirth} operation="yearBirth"/>
                                                                </div>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-3 col-lg-3">
                                                            <label className="h5 text-muted" htmlFor="CurrentPassword">Gender</label>
                                                        </div>
                                                    
                                                        <div className="col-sm-12 col-md-9 col-lg-9 text-left">
                                                            <FormRadioField callBackSelectGender={this.selectGender} selectGender={this.state.gender}/>
                                                            <div className="form-group mt-32 pb-1">
                                                                {this.state.gender==null?<p className="error">Please indicate gender.</p>:''}
                                                            </div>
                                                        </div>
                                                    </div>    
                                                    <hr />
                                                    <div className="row pt-2">
                                                        <div className="col-md-8">
                                                            <p  className="h6 text-right lh1 text-uppercase"> <NavLink to="/account-overview" >Cancel</NavLink></p>
                                                        </div>
                                                        <div className="col-md-4">
                                                            <button type="submit" disabled={isLoading} className="btn btn-success btn-block text-uppercase">Update Profile
                                                            <ClipLoader color={'#123abc'} loading={this.state.loading} />
                                                            </button>
                                                        </div>
                                                    </div>
                                                </FormWithConstraints>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

EditProfile.propTypes = {
    user: PropTypes.object.isRequired,
    editProfile: PropTypes.func.isRequired
};

export default EditProfile;
