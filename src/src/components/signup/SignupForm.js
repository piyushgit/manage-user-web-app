import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import ReCAPTCHA from 'react-google-recaptcha';
import { FormWithConstraints, FieldFeedback } from 'react-form-with-constraints';
import {
  FieldFeedbacks,
  FormGroup,
  FormControlLabel,
  FormControlInput
} from 'react-form-with-constraints/lib/Bootstrap4';
import FormSelectField from '../common/FormSelectField';
import FormRadioField from '../common/FormRadioField';
import Message from '../alert/Message';
import { ClipLoader } from 'react-spinners';

class SignupForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      dayBirth: '',
      monthBirth: '',
      yearBirth: '',
      password: '',
      passwordConfirm: '',
      gender: '',
      termsConditions: false,
      isLoading: false,
      loading: false,
      message: '',
      valid: true,
      error: false,
      gRecaptchaResponse: null
    };
    this.onChange = this.onChange.bind(this);
    this.onChecked = this.onChecked.bind(this);
    this.selectMonth = this.selectMonth.bind(this);
    this.selectMonth = this.selectMonth.bind(this);
    this.selectYear = this.selectYear.bind(this);
    this.selectDay= this.selectDay.bind(this);
    this.selectGender = this.selectGender.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.onCaptcha = this.onCaptcha.bind(this);
    //this.grecaptchaObject = grecaptcha;
    this.formWithConstraints = this.formWithConstraints.bind(this);
    this.whenNumber = this.whenNumber.bind(this);
    this.whenSmallLetters = this.whenSmallLetters.bind(this);
    this.whenCapitalLetters = this.whenCapitalLetters.bind(this);
    this.whenSpecial = this.whenSpecial.bind(this);
    this.whenSamePassword = this.whenSamePassword.bind(this);
    this.innerRef = this.innerRef.bind(this);
    this.whenYear = this.whenYear.bind(this);
    this.whenDay = this.whenDay.bind(this);
  }

  onChange(e) {
    const target = e.currentTarget;

    this.form.validateFields(target);

    this.setState({
      [target.name]: target.value,
      isLoading: !this.form.isValid()
    });

  }

  selectMonth(selectMonth) 
  { 
    this.setState({monthBirth: selectMonth, valid:true});
  }

  selectDay(selectDay)
  {
    this.setState({dayBirth: selectDay, valid:true});
  }

  selectYear(selectYear)
  {
    this.setState({yearBirth: selectYear, valid:true});
  }

  onChecked(e){
    const target = e.currentTarget;
    this.form.isValid();
    if(this.state.termsConditions !== e.target.checked){
    this.setState({termsConditions: e.target.checked, error: !e.target.checked, message: (!e.target.checked)?'Please check terms and conditions':'', isLoading: this.form.isValid()?!e.target.checked:true});
    }
  }

  selectGender(selectGender) 
  {  
    this.form.isValid();
    this.setState({gender: selectGender, isLoading: !this.form.isValid()});    
  }

  onPasswordChange(e) {
    this.form.validateFields('passwordConfirm');
    this.onChange(e);
  }

  onCaptcha(response) {
    this.setState({
      gRecaptchaResponse: response
    });
  }

  onSubmit(e) 
  {
    const {gRecaptchaResponse, termsConditions, gender, monthBirth} = this.state;
    e.preventDefault();
  
    (this.state.gender!='')?this.setState({gender:this.state.gender}):this.setState({gender:null});
    (monthBirth=='')?this.setState({valid: false}):this.setState({valid: true}) ;
    
    this.form.validateFields();

   // this.setState({ isLoading: true });

    if(this.state.gRecaptchaResponse === null)
    {
      this.setState({ error: true, message: 'Please confirm you\'re not a robot.'});
    }
    else if(this.state.termsConditions == false)
    {
      this.setState({ error: true, message: 'Please check terms and conditions'});
    }
    else
    {
      this.setState({ error: false, message: ''});
    }

    if (this.form.isValid() && gRecaptchaResponse !== null && termsConditions && gender != null && monthBirth != '') 
    {
      this.setState({loading: true, isLoading: true});
      let dob = `${this.state.yearBirth}-${this.state.monthBirth}-${this.state.dayBirth}`;
      this.props
      .userSignupRequest({
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        passwordConfirm: this.state.passwordConfirm,
        gender: this.state.gender,
        dob: dob,
        gRecaptchaResponse: this.state.gRecaptchaResponse
      })
      .then(
        response => this.setState({ message: `${this.state.firstName} successfully register`, error: false, loading: false, isLoading: false }),
        err => {
          err.response.status == 500
            ? this.setState({ message: 'Server error', error: true, loading: false, isLoading:false })
            : this.setState({ message: err.response.data.error, error: true, loading: false, isLoading:false });
        }
      );
    }
  }

  formWithConstraints(formWithConstraints)   
  {
    this.form = formWithConstraints;
  }

  innerRef(password){
    return this.password = password;
  }

  whenNumber(value){
    return !/\d/.test(value);
  }

  whenSmallLetters(value){
    return !/[a-z]/.test(value);
  }

  whenCapitalLetters(value){
    return !/[A-Z]/.test(value);
  }

  whenSpecial(value){
    return !/\W/.test(value);
  }
  
  whenSamePassword(value){
    return value !== this.password.value;
  }

  whenYear(value)
  {
    return !/^0*(18[0-9]{2}|19[0-8][0-9]|199[0-9]|200[0-9]|201[0-8])$/.test(value);
  }

  whenDay(value)
  {
    return !/^0*([1-9]|[12][0-9]|3[01])$/.test(value);
  }

  render() 
  {
    const { errors, gender } = this.state;
    return (
      <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 border-right">
        <div className="row py-2">
          <div className="col-md-12">
              <p className="h5 text-center">Sign up with your email address</p>
          </div>
        </div>
        
        {this.state.message != '' ? <Message message={this.state.message} error={this.state.error} /> : ''}
        <FormWithConstraints
          ref={this.formWithConstraints}
          onSubmit={this.onSubmit}
          noValidate
        >
        <FormGroup for="FirstName">
          <div className="input-group">
            <span className="input-group-addon">
              <i className="fa fa-user fa-fw" />
            </span>
            <FormControlInput
              type="text"
              id="firstName"
              name="firstName"
              value={this.state.firstName}
              onChange={this.onChange}
              placeholder="Enter first name"
              required
              minLength={3}
            />
          </div>
          <FieldFeedbacks for="firstName">
            <FieldFeedback when="tooShort">Too short</FieldFeedback>
            <FieldFeedback when="valueMissing">Please enter your first name.</FieldFeedback>
            <FieldFeedback when="*" />
          </FieldFeedbacks>
        </FormGroup>

        <FormGroup for="lastName">
          <div className="input-group">
            <span className="input-group-addon">
              <i className="fa fa-user fa-fw" />
            </span>
            <FormControlInput
              type="text"
              id="lastName"
              name="lastName"
              value={this.state.lastName}
              onChange={this.onChange}
              placeholder="Enter last name"
              required
              minLength={3}
            />
          </div>
          <FieldFeedbacks for="lastName">
            <FieldFeedback when="tooShort">Too short</FieldFeedback>
            <FieldFeedback when="valueMissing">Please enter your last name.</FieldFeedback>
            <FieldFeedback when="*" />
          </FieldFeedbacks>
        </FormGroup>

          <FormGroup for="email">
            <div className="input-group">
              <span className="input-group-addon">
                <i className="fa fa-envelope-o fa-fw" />
              </span>
              <FormControlInput
                type="email"
                id="email"
                name="email"
                value={this.state.email}
                onChange={this.onChange}
                placeholder="Enter email"
                required
                minLength={3}
              />
            </div>
            <FieldFeedbacks for="email">
              <FieldFeedback when="valueMissing">Please enter your email.</FieldFeedback>
              <FieldFeedback when="*" />
            </FieldFeedbacks>
          </FormGroup>

          <FormGroup for="password">
            <div className="input-group">
              <span className="input-group-addon">
                <i className="fa fa-key fa-fw"/>
              </span>
              <FormControlInput
                type="password"
                id="password"
                name="password"
                innerRef={this.innerRef}
                value={this.state.password}
                onChange={this.onPasswordChange}
                placeholder="Enter password"
                required
                pattern=".{5,}"
              />
            </div>
              <FieldFeedbacks for="password" show="all">
                <FieldFeedback when="valueMissing" />
                  <FieldFeedback when="patternMismatch">Should be at least 5 characters long</FieldFeedback>
                  <FieldFeedback when={this.whenNumber} error>
                    Should contain numbers
                  </FieldFeedback>
                  <FieldFeedback when={this.whenSmallLetters} error>
                    Should contain small letters
                  </FieldFeedback>
                  <FieldFeedback when={this.whenCapitalLetters} error>
                    Should contain capital letters
                  </FieldFeedback>
                  <FieldFeedback when={this.whenSpecial} error>
                    Should contain special characters
                </FieldFeedback>
              </FieldFeedbacks>
          </FormGroup>

          <FormGroup for="passwordConfirm">
            <div className="input-group">
              <span className="input-group-addon">
                <i className="fa fa-key fa-fw" />
              </span>
              <FormControlInput
                type="password"
                id="password-confirm"
                name="passwordConfirm"
                value={this.state.passwordConfirm}
                placeholder="Confirm password"
                onChange={this.onChange}
              />
            </div>
            <FieldFeedbacks for="passwordConfirm">
              <FieldFeedback when={this.whenSamePassword}>Not the same password</FieldFeedback>
            </FieldFeedbacks>
          </FormGroup>
         
          <div className="row">
            <div className="col-12">
              <label className="h6 text-muted" htmlFor="CurrentPassword">Date of birth</label>
            </div>
            <div className="col-sm-12 col-md-12 col-lg-12">
              <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4">
                  <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectDay} selectedValue={this.state.dayBirth} operation="dayBirth"/>
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4">
                  <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectMonth} selectedValue={this.state.monthBirth} operation="monthBirth"/>
                </div>  
            
                <div className="col-sm-12 col-md-4 col-lg-4">
                  <FormSelectField className={`form-control pr-1 pb-1 pt-1 pl-2 ${this.state.valid == false?'is-invalid':''}`}  callBackSelect={this.selectYear} selectedValue={this.state.yearBirth} operation="yearBirth"/>
                </div>  
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-12">
              <label className="h6 text-muted" htmlFor="CurrentPassword">Gender</label>
            </div>
            <div className= "col-md-12">
            <FormRadioField callBackSelectGender={this.selectGender} selectGender={this.state.gender}/>
              {gender==null?<p className="error mt-32">Please indicate gender.</p>:''}
            </div>
          </div>
    
          <div className="form-group">
            <div className="form-check">
              <label className="form-check-label">
                <input type="checkbox" className="form-check-input" name="termsConditions" checked={this.state.termsConditions} onChange={this.onChecked} />I have read the Terms and
                Conditions and Privacy and I agree.
              </label>
            </div>
          </div>

          <div className="row">
          <div className= "col-sm-12 col-md-12">
          <div className="form-group">
            <ReCAPTCHA
              ref="recaptcha"
              sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
              onChange={this.onCaptcha}
            />
          </div>
          </div>
          </div>

          <div className="form-group">
            <button disabled={this.state.isLoading} className="btn btn-success btn-block">
              SIGN UP
              <ClipLoader color={'#123abc'} loading={this.state.loading} />
            </button>
          </div>
        </FormWithConstraints>
        <div className="row">
          <div className="col-12">
            <p className="text-center">Already have an account? <NavLink to="/login">Log in</NavLink></p>
          </div>
        </div>
      </div>
    );
  }
}

SignupForm.propTypes = {
  userSignupRequest: PropTypes.func
};

export default SignupForm;
