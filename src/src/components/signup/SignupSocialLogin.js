import React from 'react';
import PropTypes from 'prop-types';
import SocialLoginButtonList from '../login/SocialLoginButtonList';

export const SignupSocialLogin = () =>
{
    return(
        <div className="col-md-6">
            <div className="row py-2">
            <div className="col-md-12">
                <p className="h5 text-center">For one time signup use any of the social link below.</p>
            </div>
            </div>
            <SocialLoginButtonList />
        </div>
    );
};