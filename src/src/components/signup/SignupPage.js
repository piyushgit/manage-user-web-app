import React from 'react';
import PropTypes from 'prop-types';
import SignupForm from './SignupForm';
import {connect} from 'react-redux';
import {userSignupRequest}  from '../../actions/signupActions';
import {SignupSocialLogin} from './SignupSocialLogin';
class SignupPage extends React.Component 
{
    render() 
    {
        const {userSignupRequest} = this.props;
        return (
            <div className="row py-4 justify-content-md-center">
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="card py-2 px-4">
                        <div className="card-block">
                            <div className="row py-2">
                                <div className="col-md-12">
                                <p className="h4 text-center">Join us!</p>
                                </div>
                            </div>
                            
                            <div className="row">
                                <SignupForm userSignupRequest={userSignupRequest}/>
                                <SignupSocialLogin />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

SignupPage.propTypes = 
{
    userSignupRequest: PropTypes.func.isRequired
};

export default connect(null, {userSignupRequest})(SignupPage);