import React from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter, history, location} from 'react-router-dom';
import Header from './common/Header';
import Main from "./main";
import {checkCurrentUser} from "../actions/loginActions";
import ProfileSideNav from './profile/ProfileSideNav';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
            navStatus: 'close',
            hideNav: false
        };
        this.navStatus = this.navStatus.bind(this);
    }

    componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
}

resize() {
    this.setState({hideNav: window.innerWidth <= 760});
}

    navStatus(navStatus){
       
        this.setState({'navStatus': navStatus});
         //console.log(this.state);
    }
    render() {
        let authToken = localStorage.getItem("userAuthToken");
        const isAuthenticated = (authToken?true:false);
        
        const {history, location, checkCurrentUser}=this.props;
        const {hideNav} = this.state;
        
        return (
            <div>
                <Header history={history} callBackNavStatus={this.navStatus} location={location} />
                {(hideNav && isAuthenticated )?<ProfileSideNav navStatus={this.state.navStatus} location={location}/>:''}
                <Main/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return({
        users: state.users
    });
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({checkCurrentUser}, dispatch); 
};

App.propTypes = {
    history: PropTypes.object,
    location: PropTypes.object,
    checkCurrentUser: PropTypes.func
};

export default withRouter(connect(mapStateToProps, {checkCurrentUser})(App));