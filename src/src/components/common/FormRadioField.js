import React from 'react';
import PropTypes from 'prop-types';

class FormRadioField extends React.Component
{
    constructor(props) {
    super(props);
  
    this.onGender = this.onGender.bind(this);
  }

    onGender(e)
    {
        const target = e.currentTarget;
        this.props.callBackSelectGender(target.value); 
    }

    render(){   
        let i = 1;

        let genderRadio = this.props.genderData.map((data)=>
             <label key={i++} className="radio-inline mr-3"><input type="radio" name="gender" value={data.value} onChange={this.onGender} checked={data.value === this.props.selectGender} /> {data.label}</label>
        );

        return(
            <fieldset className="form-group">
                {genderRadio}
            </fieldset>
        );
    }
}

FormRadioField.defaultProps = {
    genderData: [
        {
            label: "Male",
            value: "2"
        },
        {
            label: "Female",
            value: "3"
        },
        {
            label: "Non-Binary",
            value: "1"
        }
    ]
};

FormRadioField.propTypes = {
    callBackSelectGender: PropTypes.func,
    genderData: PropTypes.array.isRequired,
    selectGender: PropTypes.string
};

export default FormRadioField ;