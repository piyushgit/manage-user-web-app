import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, Redirect, history } from 'react-router-dom';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { logout } from '../../actions/loginActions';
import ProfileSideNav from '../profile/ProfileSideNav';
import {getUserFeed} from '../../utils/apiCall';

class Header extends React.Component
{
    constructor(props) {
    super(props);
    this.state =
        {
            authUser: '',
            dropdownOpen: false,
            navStatus: true
        };
        this.toggle = this.toggle.bind(this);
        this.logout = this.logout.bind(this);
        this.onSideNav = this.onSideNav.bind(this);
        this.closeDropdown = this.closeDropdown.bind(this);
    }

    componentWillMount() 
    {
        let userAuthToken = localStorage.getItem("userAuthToken");
        
        if(userAuthToken)
        {
            getUserFeed(userAuthToken).then(
                (res)=>{this.setState({authUser: res.data.first_name});},
                (err)=>{this.props.logout(userAuthToken);}
            );
        }
    }

    onSideNav(){
         this.setState({navStatus: !this.state.navStatus});
         
        this.props.callBackNavStatus(this.state.navStatus?'open':'close');
    }

    closeDropdown(){
        this.setState({dropdownOpen: false,  navStatus: true});
    }

    toggle() {
        this.setState({
        dropdownOpen: !this.state.dropdownOpen
        });
    }

    logout(e) {
        this.setState({dropdownOpen: false});
        let userAuthToken = localStorage.getItem("userAuthToken");
        e.preventDefault();
        this.props.logout(userAuthToken);
    }

    render()
    {
        
        let authToken = localStorage.getItem("userAuthToken");
        const isAuthenticated = (authToken?true:false);

        const {redirect, authUser} = this.state;

        let nav = '';
        let userProfile = '';

        const userLinks = (
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link">Home</NavLink>
                    </li>
                </ul>
                <ul className="navbar-nav ml-auto">          
                    <li className="nav-item ml-auto"> 
                        <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                            <DropdownToggle caret>
                            Profile
                            </DropdownToggle>
                            <DropdownMenu>
                                <NavLink to="/account-overview" className="dropdown-item" onClick={this.closeDropdown}>
                                <i className="fa fa-home iSideNav"  aria-hidden="true"></i>
                                Overview</NavLink>
                                <NavLink to="/edit-profile" className="dropdown-item" onClick={this.closeDropdown}>
                                <i className="fa fa-pencil iSideNav"  aria-hidden="true"></i>
                                Edit profile</NavLink>
                                <NavLink to="/change-password" className="dropdown-item" onClick={this.closeDropdown}>
                                <i className="fa fa-lock iSideNav" aria-hidden="true"></i>
                                Change password</NavLink>
                                <DropdownItem divider />
                                <NavLink to="#" className="dropdown-item" onClick={this.logout}>
                                <i className="fa fa-sign-out iSideNav" aria-hidden="true"></i>
                                Logout</NavLink>
                            </DropdownMenu>
                        </ButtonDropdown>
                    </li>
                </ul>
            </div>
        );

        const guestLinks = (
            <div className={`${(this.state.navStatus==false && isAuthenticated==false)?'show':''} collapse navbar-collapse`}>
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <NavLink to="/login" className="nav-link" onClick={this.closeDropdown}>Login</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/signup" className="nav-link" onClick={this.closeDropdown}>Sign Up</NavLink>
                    </li>
                </ul>
            </div>
        );

        return (
        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={this.onSideNav}>
            <span className="navbar-toggler-icon"></span>
        </button>
            <NavLink className="navbar-brand" to="/">User Management</NavLink>
            {(isAuthenticated)?userLinks:guestLinks}
        </nav>
        );
        
    }
}

Header.propTypes = {
    auth: PropTypes.object.isRequired,
    logout: PropTypes.func,
    callBackNavStatus: PropTypes.func
};

function mapStateToProps(state) {
  return {
    auth: {}
  };
}

export default connect(mapStateToProps, {logout})(Header);