import { getLoadedConfig } from './config-loader';

const config = getLoadedConfig();

export default config;
