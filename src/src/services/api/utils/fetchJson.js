const handleSuccess = (response, resolveFunc) => {
    const responseContentType = response.headers.get('Content-Type');
    if (responseContentType && responseContentType.indexOf('application/json') !== -1) {
        response.json().then(resolveFunc);
    } else {
        resolveFunc();
    }
};

const handleError = (response, rejectFunc) => {
    const responseContentType = response.headers.get('Content-Type');
    if (responseContentType.indexOf('text/plain') !== -1) {
        response.text().then(text => rejectFunc({ message: text }));
    } else if (responseContentType.indexOf('application/json') !== -1) {
        response.json().then(rejectFunc);
    } else {
        rejectFunc({ message: 'An error occured' });
    }
};

const fetchJson = (input, init) => {
    return new Promise((resolve, reject) => {
        const headers = {
            'Content-Type': 'application/json'
        };
        fetch(input, {
            ...init,
            credentials: 'include',
            headers
        }).then(response => {
            if (response.ok) {
                handleSuccess(response, resolve);
            } else {
                handleError(response, reject);
            }
        });
    });
};

export default fetchJson;
