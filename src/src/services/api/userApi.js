import config from '../config';
import fetchJson from './utils/fetchJson';

export const fetchUsers = () => {
    return fetchJson(
        `${config.userApiUrl}/get`
    ).catch(error => {
        throw new Error('Failed to get users');
    });
};