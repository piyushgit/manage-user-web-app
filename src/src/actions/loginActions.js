import axios from 'axios';
import {SET_CURRENT_USER, CHECK_CURRENT_USER} from './type';
import { setAuthorizationToken } from '../shared/setAuthorizationToken';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const userLoginRequest = (userData) => {
    //console.log(userData);
    return dispatch => {
        return axios.post(`${baseUrl}/users/login`, userData);
    };
};

//let user = JSON.parse(localStorage.getItem('loggedUser'));

/*
 *
 * Set logged user to system
 *         
 */
export const setCurrentUser = (user) => {
    // user = {"firstName": user.first_name, "lastName": user.last_name, "email": user.email};
    return (
        {
            type: SET_CURRENT_USER,
            user
        }
    );
};

export const checkCurrentUser = (response) => {
    return (
        {
            type: CHECK_CURRENT_USER,
            response
        }
    );
};

export function logout(token) {
    return dispatch => {
        return axios.post(`${baseUrl}/users/logout?token=${token}`).then(
            (response) => { localStorage.clear(); dispatch(setCurrentUser({})); },
            (error) => { if (error.response.status != 500) { localStorage.clear(); dispatch(setCurrentUser({})); } }
        );
    };
}

export const userLoggedDetail = (accessToken) => {
    return dispatch => {
        return axios.get(`${baseUrl}/users/me/profile?token=${accessToken}`).then((res) => {
            localStorage.setItem('userAuthToken', accessToken);
            dispatch(setCurrentUser(res.data));
        });
    };
};