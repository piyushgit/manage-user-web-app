import axios from 'axios';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const forgotPasswordRequest = (userData) => {
    return (dispatch) => {
        return axios.post(`${baseUrl}/users/password/request/`, userData);
    };
};

export const forgotPassword = (userData) => {
    let token = userData.token.replace(':', '');
    return (dispatch) => {
        //Piyush - pass the token in the request payload.
        return axios.post(`${baseUrl}/users/password/reset/?token=${token}`, { "token":token, "passwordConfirm": userData.passwordConfirm });
    };
};
