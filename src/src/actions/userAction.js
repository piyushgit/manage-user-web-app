import axios from 'axios';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

const userGetRequest = (userData) => {
    return (dispatch) => {
        return axios.get(`${baseUrl}/user`, userData)
        .then(function(response){
            
        });
    };
};

export default userGetRequest;