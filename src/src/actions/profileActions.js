import React from 'react';
import axios from 'axios';
import {EDIT_CURRENT_USER} from './type';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const passwordChange = (userData) => {
  let token = localStorage.getItem('userAuthToken');
    return dispatch => {
        return  axios.post(`${baseUrl}/users/me/password?token=${token}`, userData);
    };
};

export const editProfile = (userData) => {
    let token = localStorage.getItem('userAuthToken');
    return dispacth => {
        return axios.post(`${baseUrl}/users/me/profile?token=${token}`, userData);
    };
    
};