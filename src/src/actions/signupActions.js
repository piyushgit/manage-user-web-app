import axios from 'axios';

import config from '../data/env';
const baseUrl = config[0].userApiUrl;

export const userSignupRequest = (userData) => {
    return (dispatch) => {
        return axios.post(`${baseUrl}/users`, userData);
    };
};

export const signupConfirmationVerify = (verifyToken) => {
    return (dispatch) => {
        //Piyush - this needs to be tested
        return axios.post(`${baseUrl}/users/confirm/?verificationToken=${verifyToken}`);
    };
};

export const resendSignupVefification = (email) => {
    return (dispatch) => {
        return axios.post(`${baseUrl}/users/resend/email?email=${email}`);
    };
};

