import 'react-hot-loader/patch';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import queryString from 'query-string';
import { AppContainer } from 'react-hot-loader';
import store from './redux/store';
import Router from './router';

// import routes from './routes';

import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap-social/bootstrap-social.css';
import '../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import '../node_modules/react-bootstrap-toggle/dist/bootstrap2-toggle.css';
import { setCurrentUser } from './actions/loginActions';
import './styles/styles.css';


const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <Component />
            </Provider>
        </AppContainer>,
        document.getElementById('root')
    );
};

render(Router);

if (module.hot) {
    module.hot.accept('./router', () => {
        const NextRouter = require('./router').default;
        render(NextRouter);
    });
}


