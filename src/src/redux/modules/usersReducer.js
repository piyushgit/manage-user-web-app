import { SET_CURRENT_USER, CHECK_CURRENT_USER } from '../../actions/type';
import isEmpty from 'lodash/isEmpty';
import axios from 'axios';

export default (state=[], action) =>
{
    const i = action.index;
    let newStatus = '';
    let userData;
    let loggedUser;
    let dob;
    let dobArray;
    let yearBirth;
    let monthBirth;
    let dayBirth;
    let gender;
    
    switch(action.type)
    {
        case 'CHANGE_USER_STATUS':
        
            if(state[i].status=='active')
            {
                newStatus = 'unable';
            }
            else
            {
                newStatus = 'active';
            }
            return ([
                ...state.slice(0, i),
                {...state[i], status: newStatus},
                ...state.slice(i+1)
            ]);
      
        case SET_CURRENT_USER:
            dob = action.user.dob;
            dobArray = (dob)?dob.split("-"):'';
            yearBirth = dobArray[0];
            monthBirth = dobArray[1];
            dayBirth = dobArray[2];

            if(action.user.gender == 'male')
            {
                gender = '2';
            }
            else if(action.user.gender == 'female')
            {
                gender = '3';
            }
            else 
            {
                gender = '1';
            }

            userData = {"firstName": action.user.first_name, "lastName": action.user.last_name, "email": action.user.email, yearBirth, monthBirth, dayBirth, gender};
            loggedUser = localStorage.setItem('loggedUser', JSON.stringify(userData));
            return {
                isAuthenticated: !isEmpty(action.user),
                user: action.user
            };

            case CHECK_CURRENT_USER:
            return  {
                status: 'loggedIn',
                user: 'attach'
            };
               
        default:
        return state;
    }   
};
