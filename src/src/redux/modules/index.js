import { combineReducers } from 'redux';
import users from './usersReducer';
import {profile} from './profileReducer';

const allReducers = combineReducers(
  {
    users,
    profile
  }
);

export default allReducers;